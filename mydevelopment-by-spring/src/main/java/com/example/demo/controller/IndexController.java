package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class IndexController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("index");
		return model;
	}

	@RequestMapping(value = "/NewRegist", method = RequestMethod.GET)
	public ModelAndView NewRegist( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("NewRegist");
		return model;
	}

	@RequestMapping(value = "/NewRegist_Common", method = RequestMethod.GET)
	public ModelAndView NewRegist_Common( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("NewRegist_Common");
		return model;
	}

	@RequestMapping(value = "/NewRegist_Artist", method = RequestMethod.GET)
	public ModelAndView NewRegist_Artist( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("NewRegist_Artist");
		return model;
	}



	@RequestMapping(value = "/SearchResult", method = RequestMethod.POST)
	public ModelAndView SearchResult( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("SearchResult");
		return model;
	}

	@RequestMapping(value = "/ArtistPage", method = RequestMethod.GET)
	public ModelAndView ArtistPage( ModelAndView model) {

		// 画面描画用のテンプレート名を指定
		model.setViewName("ArtistPage");
		return model;
	}

}
