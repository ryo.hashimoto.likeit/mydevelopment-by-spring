package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@RequestMapping(value = "/Login", method = RequestMethod.GET)
	public ModelAndView Login( ModelAndView model) {

		model.setViewName("Login");
		return model;
	}

	@RequestMapping(value = "/Login", method = RequestMethod.POST)
	public ModelAndView LoginMyPage( ModelAndView model) {

		model.setViewName("MyPage");
		return model;
	}

}
