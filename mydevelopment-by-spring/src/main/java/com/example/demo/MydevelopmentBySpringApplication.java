package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MydevelopmentBySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MydevelopmentBySpringApplication.class, args);
	}

}
